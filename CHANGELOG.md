# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.2] - 2023-11-05

- Fix `lissy.pairs`: Do not insert space before, when at end of file.
- Fix `lissy.line-break`: Do indent by one, when at source root and infront of
  the first expression.
- Fix `lissy.line-break`: When inside comment, do not align to start of comment,
  but relative to parent expression.

## [0.4.1] - 2023-10-01

- Make it impossible to type normal characters while at a special position. This
  is more consistent, because some keys trigger lissy's special commands.
- Fix `lissy.slurp`: gets stuck when slurping comments
- Fix `lissy.delete-backward`: should not delete comments which end with `)`

## [0.4.0] - 2023-09-24

- Add `lissy.indent`: indent the current expression

## [0.3.1] - 2023-09-24

- `lissy.delete-backward` (backspace) after opening paren now deletes the whole
  expression. Keeping the balance intact.
- Make it impossible to replace selections directly. This is more consistent,
  because some keys trigger lissy's special commands.
  To replace selected text, explicitly delete it first.
- Fix `lissy.pair`: should not add space when inserting opening parens at the
  end of comments, or after #.

## [0.3.0] - 2023-09-17

- Automatic indentation (when inserting line breaks).
  The new options `lissy.macro-indents` and `lissy.extra-macro-indents` can be
  used to configure which forms should be considered as "forms with body".
- Fix `lissy.forward` and `lissy.backward` when on space between expressions

## [0.2.0] - 2023-09-16

- Use simpler s-expression tree-sitter grammar. Should be more error resistant.
- Add `lissy.colon`: insert colon, and space after, if appropiate
- Add `lissy.comment`: comment the current expression
- Add `lissy.select`: select the current expression
- Fix `lissy.pair` in front of symbols

## [0.1.0] - 2023-09-12

- Initial release
