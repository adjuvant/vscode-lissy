# Lissy

Structural editing for Fennel.

Heavily inspired by the Emacs package [lispy](https://github.com/abo-abo/lispy).
Have a look at its README for an overview of theoretically possible commands,
of which most are not (yet) implemented here.
And those that are behave slightly differently. Have fun!

## Status

Experimental.

Currently, `fennel` is the only supported language.
But it should not be hard to add support for other Lisp-like languages in the future.

This extension does not add Fennel language support.
Another extension is required! For example `kongeor.vsc-fennel`, or [`adjuvant.fennel-syntax`](https://codeberg.org/adjuvant/vscode-fennel-syntax)

Most of the heavy lifting is done by [tree-sitter](https://tree-sitter.github.io/tree-sitter/)
with a [simple s-expression grammar for Fennel](https://codeberg.org/adjuvant/tree-sitter-sexp-fennel).
The simple grammar wont provide much detail about the code,
but is much more error resistant, than a complete Fennel grammar.
Only worry about unterminated strings.

## Command reference

I am too lazy to list all commands here. And would probably forget to update this list anyway.
Please have a look at the source code instead: https://codeberg.org/adjuvant/vscode-lissy/src/branch/main/src/keymap.ts

You can read more about how the commands **should** behave in the
Lispy function reference: http://oremacs.com/lispy/

## Warning: VSCode 'type' command

This extension registers the special `type` command.
Only one extension can do that at a time.
But, as far as I am aware, this is the only way to bind to non-alphanumeric characters
(independent of keyboard layout).

TODO: Do not register `type` command if no keybindings are configured.
      That is `base-keymap` is set to `none` and `global-keys` and `special-keys` are empty.

## Configuration

Two keymaps:
- global: always active
- special: only active if the [the point is special](https://github.com/abo-abo/lispy#operating-on-lists) (e.g. before or after parens)

For example: To remove the default mapping of `[`, and override the mapping of `]` and add a mapping for `}`:

```json
  "lissy.global-keys": {
    "[": null,
    "]": "lissy.backward",
    "}": "lissy.forward"
  },
```

You can also bind `lissy.` commands to normal VSCode hotkeys.
Just make sure to add `"when": "editorTextFocus && editorLangId === fennel"`

To deactivate the default mappings, set `lissy.base-keymap` to `"none"`.

## Known problems

- It is not possible to directly replace selected text by typing. As some keys
  would trigger lissy special commands. Delete the existing text first.
- `lissy.comment` comments whole lines, causing collateral damage.
- `lissy.indent` does not yet work in combination with multi-line strings
- `lissy.indent` indents all "macro bodies" the same way. That is, `if` and `do`
  indentation is identical to `fn` and `λ`, which can be wrong:
  ```
  (if (= a 2)
    true
    (= a 3)
    3)
  ```
  ```
  (do (set a 1)
    (set b 2))
  ```

## Differences to lispy

- `lissy.indent` instead of `lispy-tab`, only indents, nothing else.

## TODO

- Fix: Cursor before : in (foo:bar 42) should not be special
- Fix: trailing parens cause indent to do weird things (and trailing parens are not indented at all)
- Fix: indent of `do`: indent first arg by 2, and others aligned to first

- kill, yank, copy, paste, kill ring
- multi-line, single-line
- implement more of Lispy's commands
- implement Lispy's arg count, repeated commands
- write tests for command logic
- movement commands with regions (selections) should keep them active
- multiple selections
- highlight selected sexp
- automatic indentation

## Development

Start the "Run Extension" task in vscode (F5)

The `npm run watch` default build task should start automatically in the background.

### Installation

```
npm install --ignore-scripts
npx vsce package
codium --install-extension lissy-x.y.z.vsix
```

## Source

The source code of this extension is available at https://codeberg.org/adjuvant/vscode-lissy

## License

The source code of this extension is licensed under the **Mozilla Public License
Version 2.0**. See file LICENSE.

See `grammars/` for bundled grammars and their licenses.
- [tree-sitter-fennel](https://github.com/TravonteD/tree-sitter-fennel) : MIT
