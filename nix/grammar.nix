{ pkgs ? import <nixpkgs> { } }:

let
  mkGrammar = { id, src }:
    pkgs.runCommand "tree-sitter-${id}"
      {
        buildInputs = [
          pkgs.tree-sitter
          pkgs.emscripten
        ];
      } ''
      cp -r ${src}/* .
      tree-sitter build-wasm
      mkdir $out
      cp *.wasm $out
      cp LICENSE $out/tree-sitter-${id}.LICENSE
    '';

  fennel-src = pkgs.fetchgit {
    url = "https://github.com/TravonteD/tree-sitter-fennel/";
    rev = "517195970428aacca60891b050aa53eabf4ba78d";
    sha256 = "sha256-7bmrLJunNAus8XbBcBiTS5enhSzZ1mecAAyqlZUtSgo=";
  };
  fennel-grammar = mkGrammar { id = "fennel"; src = fennel-src; };

  sexp-fennel-src = pkgs.fetchgit {
    url = "https://codeberg.org/adjuvant/tree-sitter-sexp-fennel";
    rev = "d03ad984040b84b89d7c234fda9e866fe3a28689";
    sha256 = "sha256-BaUQ5U3aIr7XkKukxe8x+5uXdsFylIhZp5qjK6qjah0=";
  };
  sexp-fennel-grammar = mkGrammar { id = "sexp-fennel"; src = sexp-fennel-src; };
in
sexp-fennel-grammar
