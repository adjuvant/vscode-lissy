/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import * as vscode from "vscode"
import TreeSitter from "web-tree-sitter"

export interface State {
    parser: TreeSitter
    parseTrees: Map<vscode.TextDocument, TreeSitter.Tree>
    globalKeymap: Map<string, string>
    specialKeymap: Map<string, string>
    macroIndents: Map<string, boolean>,
    backHistory: Array<vscode.Selection>
    currentKey?: string,
}

export function parse(state: State, document: vscode.TextDocument): TreeSitter.Tree {
    // TODO: Reuse old trees
    // But needs to records edits: onDocumentChange -> tree.edit(...)
    // https://github.com/tree-sitter/tree-sitter/tree/master/lib/binding_web#editing
    const text = document.getText()
    //const previousTree = state.parseTrees.get(document)
    //console.time("parsing")
    const tree = state.parser.parse(text) //, previousTree)
    //console.timeEnd("parsing")
    //state.parseTrees.set(document, tree)
    return tree
}
