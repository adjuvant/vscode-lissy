/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import * as vscode from "vscode"
import TreeSitter from "web-tree-sitter"
import { State } from "./state"
import { makeCommands } from "./commands"
import * as keymap from "./keymap"

export async function activate(context: vscode.ExtensionContext) {
    await TreeSitter.init({
        locateFile(scriptName: string, scriptDirectory: string) {
            return context.asAbsolutePath("node_modules/web-tree-sitter/" + scriptName)
        },
    })

    const parser = new TreeSitter()
    const lang = await TreeSitter.Language.load(context.asAbsolutePath("grammars/tree-sitter-sexp_fennel.wasm"))
    parser.setLanguage(lang)

    const state: State = {
        parser,
        parseTrees: new Map(),
        globalKeymap: keymap.getGlobalKeymap(),
        specialKeymap: keymap.getSpecialKeymap(),
        macroIndents: getMacroIndents(),
        backHistory: [],
    }

    vscode.workspace.onDidChangeConfiguration((change) => {
        if (change.affectsConfiguration("lissy")) {
            state.globalKeymap = keymap.getGlobalKeymap()
            state.specialKeymap = keymap.getSpecialKeymap()
            state.macroIndents = getMacroIndents()
        }
    }, context.subscriptions)

    const commands = makeCommands(state)
    Object.entries(commands).forEach(([key, value]) => {
        // XXX: This extension registers the `type` command to handle keybindings.
        // This is necessary to support non-alphanumeric "keys": `(}[>`
        // But only one extension can register the `type` command at a time!
        // TODO: detect if "type" is already registered, and fail gracefully?
        const command = (key !== "type" ? "lissy." : "") + key
        context.subscriptions.push(
            // XXX: Cannot use `registerTextEditorCommand` for commands with return value,
            //      or the result wont be returned. But, do we really need to return anything?
            vscode.commands.registerCommand(command, value)
        )
    })
}

export function deactivate(): Thenable<void> | undefined {
    return
}

function spaceSeparatedConfig(id: string): string[] {
    const section = vscode.workspace.getConfiguration("lissy")
    const config = section.get(id)
    if (typeof config !== "string") {
        throw new Error(`Configuration fennel-ls.${id} must be a string.`)
    }
    return config.split(" ").filter((s) => s !== "")
}

function getMacroIndents(): Map<string, boolean> {
    // TODO: ask the language server
    const defaultIndents = spaceSeparatedConfig("macro-indents")
    const extraIndents = spaceSeparatedConfig("extra-macro-indents")
    const indents = new Map()
    defaultIndents.forEach((name) => indents.set(name, true))
    extraIndents.forEach((name) => indents.set(name, true))
    return indents
}
