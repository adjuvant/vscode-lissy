/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import * as vscode from "vscode"
import TS from "web-tree-sitter"
import { State, parse } from "./state"

const startCharacters = ["(", "[", "{", ":", "#", ";", '"']
const endCharacters = [")", "]", "}", '"']

function findNodeBetween(tree: TS.Tree, start: number, end: number): TS.SyntaxNode {
    const cursor = tree.walk()
    while (true) {
        if (cursor.startIndex <= start && cursor.endIndex >= end) {
            if (!cursor.gotoFirstChild()) {
                break
            }
        } else {
            if (!cursor.gotoNextSibling()) {
                cursor.gotoParent()
                break
            }
        }
    }
    return cursor.currentNode()
}

function closestNamedParent(node: TS.SyntaxNode): TS.SyntaxNode {
    while (node.parent && !node.isNamed()) {
        node = node.parent
    }
    return node
}

function findCurrentSexp(state: State, textEditor: vscode.TextEditor): TS.SyntaxNode {
    const tree = parse(state, textEditor.document)
    const start = textEditor.document.offsetAt(textEditor.selection.start)
    const end = textEditor.document.offsetAt(textEditor.selection.end)
    const currentNode = findNodeBetween(tree, start, end)
    let currentSexp = closestNamedParent(currentNode)
    // XXX: For example: Cursor is directly between two opening parens: `(|(bar ))`, or `{|:foo 1}`
    if (
        start === end &&
        currentNode.endIndex === start &&
        currentSexp.firstNamedChild &&
        currentSexp.firstNamedChild.startIndex === end
    ) {
        currentSexp = currentSexp.firstNamedChild
    }
    return currentSexp
}

function findCurrentSexpBefore(state: State, textEditor: vscode.TextEditor): TS.SyntaxNode {
    const tree = parse(state, textEditor.document)
    const position = textEditor.document.offsetAt(textEditor.selection.start)
    const currentNode = findNodeBetween(tree, position, position)
    const container = closestNamedParent(currentNode)
    if (position > container.startIndex && position < container.endIndex) {
        for (let i = container.namedChildCount - 1; i >= 0; i--) {
            const child = container.namedChildren[i]
            if (child.startIndex < position && isSpecialNode(child, "start")) {
                return child
            }
        }
    }
    return container
}

function findCurrentSexpAfter(state: State, textEditor: vscode.TextEditor): TS.SyntaxNode {
    const tree = parse(state, textEditor.document)
    const position = textEditor.document.offsetAt(textEditor.selection.end)
    const currentNode = findNodeBetween(tree, position, position)
    const container = closestNamedParent(currentNode)
    if (position > container.startIndex && position < container.endIndex) {
        for (const child of container.namedChildren) {
            if (child.endIndex > position && isSpecialNode(child, "end")) {
                return child
            }
        }
    }
    return container
}

function selectAndRevealOffset(textEditor: vscode.TextEditor, offset: number) {
    const position = textEditor.document.positionAt(offset)
    textEditor.selection = new vscode.Selection(position, position)
    textEditor.revealRange(textEditor.selection)
}

function withTextEditor(
    accept: (textEditor: vscode.TextEditor, ...args: any[]) => any,
    deny?: (...args: any[]) => any
): (...args: any[]) => any {
    return (...args) => {
        const textEditor = vscode.window.activeTextEditor
        if (!textEditor) {
            throw new Error("Invalid command call without active text editor!")
        }
        if (textEditor.document.languageId !== "fennel") {
            return deny ? deny(...args) : undefined
        }
        return accept(textEditor, ...args)
    }
}

function isSpecialAfterPosition(textEditor: vscode.TextEditor, position: vscode.Position): boolean {
    const range = new vscode.Range(position, new vscode.Position(position.line, position.character + 1))
    const text = textEditor.document.getText(range)
    return startCharacters.includes(text)
}

function isSpecialCharacterBefore(textEditor: vscode.TextEditor, index: number): boolean {
    const position = textEditor.document.positionAt(index)
    const range = new vscode.Range(new vscode.Position(position.line, position.character - 1), position)
    const text = textEditor.document.getText(range)
    return endCharacters.includes(text)
}

function isSpecialCharacterAfter(textEditor: vscode.TextEditor, index: number): boolean {
    const position = textEditor.document.positionAt(index)
    const range = new vscode.Range(position, new vscode.Position(position.line, position.character + 1))
    const text = textEditor.document.getText(range)
    return startCharacters.includes(text)
}

// TODO: export as `lissy.special` when clause context: https://code.visualstudio.com/api/references/when-clause-contexts#add-a-custom-when-clause-context
// - conditional keybindings
// - hide unuseable commands
function isBeforeOrAfterParen(textEditor: vscode.TextEditor): boolean {
    const caret = textEditor.selection.start
    if (caret.character > 0) {
        const before = new vscode.Range(new vscode.Position(caret.line, caret.character - 1), caret)
        const textBefore = textEditor.document.getText(before)
        if (endCharacters.includes(textBefore)) {
            return true
        }
    }
    const after = new vscode.Range(caret, new vscode.Position(caret.line, caret.character + 1))
    const textAfter = textEditor.document.getText(after)
    if (startCharacters.includes(textAfter)) {
        return true
    }
    return false
}

/** Some nodes are only special at the start: :shorthand ;; comment */
function isSpecialNode(node: TS.SyntaxNode, side: "start" | "end" = "start"): boolean {
    if (side === "start") {
        return startCharacters.includes(node.text.charAt(0))
    } else {
        return node.type !== "comment" && endCharacters.includes(node.text.charAt(node.text.length - 1))
    }
}

function positionEqualsPoint(position: vscode.Position, point: TS.Point): boolean {
    return position.line === point.row && position.character === point.column
}

function inStringOrComment(state: State, textEditor: vscode.TextEditor): boolean {
    const node = findCurrentSexp(state, textEditor)
    const atStart = positionEqualsPoint(textEditor.selection.start, node.startPosition)
    const atEnd = positionEqualsPoint(textEditor.selection.end, node.endPosition)
    return !atStart && ((!atEnd && node.type === "string" && node.text.charAt(0) !== ":") || node.type === "comment")
}

function isSelectionAtStartOfNode(textEditor: vscode.TextEditor, node: TS.SyntaxNode): boolean {
    const startOffset = textEditor.document.offsetAt(textEditor.selection.start)
    return node.startIndex === startOffset
}

function rangeOfNode(node: TS.SyntaxNode): vscode.Range {
    return new vscode.Range(
        node.startPosition.row,
        node.startPosition.column,
        node.endPosition.row,
        node.endPosition.column
    )
}

function listIndentation(state: State, parent: TS.SyntaxNode, position: number): number {
    const firstChild = parent.firstNamedChild
    // Before name?
    if (!firstChild || position < firstChild.endIndex) {
        return 1
    }
    // Is macro with body?
    if (state.macroIndents.has(firstChild.text)) {
        return 2
    }
    const secondChild = firstChild.nextNamedSibling
    // Before first argument?
    if (!secondChild || position < secondChild.endIndex) {
        return firstChild.startPosition.column - parent.startPosition.column
    }
    return secondChild.startPosition.column - parent.startPosition.column
}

// TODO: Test and cleanup ...
function relativeIndentsOfNode(state: State, node: TS.SyntaxNode): number[] {
    if (node.startPosition.row === node.endPosition.row) {
        return []
    }
    const indents = []
    const isList = node.text.startsWith("(")
    const isCollection = node.text.startsWith("{") || node.text.startsWith("[")
    let parentIndent = node.type === "string" || node.type === "comment" ? 0 : 1
    let childIndex = 1
    let lastRow = node.startPosition.row
    let child = node.firstNamedChild
    let isMacroWithBody = false
    // FIXME: missing indents for multi-line nodes (strings)
    while (child) {
        // empty lines
        while (lastRow < child.startPosition.row - 1) {
            indents.push(-1) // no indent at all on this line
            lastRow++
        }
        if (child.startPosition.row > lastRow || child.endPosition.row > lastRow) {
            if (child.startPosition.row > lastRow) {
                indents.push(parentIndent)
            }
            const baseIndent =
                child.startPosition.row === lastRow
                    ? child.startPosition.column - node.startPosition.column
                    : parentIndent
            if (child.endPosition.row > child.startPosition.row) {
                for (const indent of relativeIndentsOfNode(state, child)) {
                    indents.push(baseIndent + indent)
                }
            }
            lastRow = child.endPosition.row
        }
        if (childIndex === 1) {
            isMacroWithBody = isList && state.macroIndents.has(child.text)
        }
        if (isMacroWithBody) {
            parentIndent = 2
        } else if (childIndex === 2 && !isCollection && child.startPosition.row === node.startPosition.row) {
            parentIndent = child.startPosition.column - node.startPosition.column
        }
        childIndex++
        child = child.nextNamedSibling
    }
    return indents
}

function indentsOfNode(state: State, node: TS.SyntaxNode) {
    return relativeIndentsOfNode(state, node).map((ind) => (ind < 0 ? 0 : ind + node.startPosition.column))
}

function executeCommand(state: State, textEditor: vscode.TextEditor, key: string): boolean | Thenable<any> {
    const { globalKeymap, specialKeymap } = state
    // Global
    let command = globalKeymap.get(key)
    if (command) {
        if (command === "::right-nostring") {
            if (inStringOrComment(state, textEditor)) {
                return false
            }
            command = "lissy.right"
        }
        return vscode.commands.executeCommand(command)
    }
    // Special
    else if (
        (isBeforeOrAfterParen(textEditor) || !textEditor.selection.isEmpty) &&
        !inStringOrComment(state, textEditor)
    ) {
        command = specialKeymap.get(key)
        return command ? vscode.commands.executeCommand(command) : true
    }
    return false
}

export function makeCommands(state: State) {
    return {
        type: withTextEditor(
            (textEditor: vscode.TextEditor, opts: { text: string }) => {
                const { text } = opts
                state.currentKey = text
                const result = executeCommand(state, textEditor, text)
                if (result === false) {
                    return vscode.commands.executeCommand("default:type", opts)
                }
                return result
            },
            (opts) => vscode.commands.executeCommand("default:type", opts)
        ),

        // --- Movement --------------------------------------------------------

        forward: withTextEditor((textEditor: vscode.TextEditor, arg: unknown) => {
            const offset = textEditor.document.offsetAt(textEditor.selection.end)
            let target = findCurrentSexpAfter(state, textEditor)
            while ((offset === target.endIndex || !isSpecialNode(target, "end")) && target.parent) {
                target = target.nextNamedSibling ?? target.parent
            }
            selectAndRevealOffset(textEditor, target.endIndex)
        }),
        backward: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const offset = textEditor.document.offsetAt(textEditor.selection.start)
            let target = findCurrentSexpBefore(state, textEditor)
            while ((offset === target.startIndex || !isSpecialNode(target, "start")) && target.parent) {
                target = target.previousNamedSibling ?? target.parent
            }
            selectAndRevealOffset(textEditor, target.startIndex)
        }),
        right: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            // FIXME: gets stuck at end of file
            const current = findCurrentSexp(state, textEditor)
            if (current.parent) {
                state.backHistory.push(textEditor.selection)
                selectAndRevealOffset(textEditor, current.parent.endIndex)
            }
        }),
        left: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            if (current.parent) {
                state.backHistory.push(textEditor.selection)
                selectAndRevealOffset(textEditor, current.parent.startIndex)
            }
        }),
        down: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const side = isSelectionAtStartOfNode(textEditor, current) ? "start" : "end"
            let next = current.nextNamedSibling
            while (next && !isSpecialNode(next, side)) {
                next = next.nextNamedSibling
            }
            if (next) {
                state.backHistory.push(textEditor.selection)
                const targetOffset = side === "start" ? next.startIndex : next.endIndex
                selectAndRevealOffset(textEditor, targetOffset)
            }
        }),
        up: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const side = isSelectionAtStartOfNode(textEditor, current) ? "start" : "end"
            let next = current.previousNamedSibling
            while (next && !isSpecialNode(next, side)) {
                next = next.previousNamedSibling
            }
            if (next) {
                state.backHistory.push(textEditor.selection)
                const targetOffset = side === "start" ? next.startIndex : next.endIndex
                selectAndRevealOffset(textEditor, targetOffset)
            }
        }),
        different: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const targetOffset = isSelectionAtStartOfNode(textEditor, current) ? current.endIndex : current.startIndex
            if (isSpecialNode(current, "end")) {
                selectAndRevealOffset(textEditor, targetOffset)
            }
        }),
        flow: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const forward = isSelectionAtStartOfNode(textEditor, current)
            let target = current
            const isSpecialChar = forward
                ? () => isSpecialCharacterAfter(textEditor, target.startIndex)
                : () => target.type !== "comment" && isSpecialCharacterBefore(textEditor, target.endIndex)
            while (
                (forward ? target.startIndex <= current.startIndex : target.endIndex >= current.endIndex) ||
                !isSpecialChar()
            ) {
                let next = forward
                    ? target.firstNamedChild || target.nextNamedSibling
                    : target.lastNamedChild || target.previousNamedSibling
                if (next) {
                    target = next
                } else {
                    while (!(forward ? target.nextNamedSibling : target.previousNamedSibling)) {
                        if (target.parent) {
                            target = target.parent
                        } else {
                            return
                        }
                    }
                    target =
                        (forward ? target.nextNamedSibling : target.previousNamedSibling) ||
                        // to please the type checker
                        target
                }
            }
            if (target !== current) {
                state.backHistory.push(textEditor.selection)
                selectAndRevealOffset(textEditor, forward ? target.startIndex : target.endIndex)
            }
        }),
        back: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const previous = state.backHistory.pop()
            if (previous) {
                textEditor.selection = previous
                textEditor.revealRange(previous)
            }
        }),

        // --- Modification ----------------------------------------------------

        "delete-backward": withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const parent = current.parent
            let toDelete = null
            if (
                // Behind current end
                (current.endPosition.row === textEditor.selection.end.line &&
                    current.endPosition.column === textEditor.selection.end.character &&
                    isSpecialNode(current, "end")) ||
                // Behind currrent start
                (current.startPosition.row === textEditor.selection.start.line &&
                    current.startPosition.column === textEditor.selection.start.character - 1 &&
                    isSpecialNode(current, "start"))
            ) {
                toDelete = current
            } else if (
                // Before current start, behind start of parent
                parent &&
                parent.startPosition.row === textEditor.selection.start.line &&
                parent.startPosition.column === textEditor.selection.start.character - 1 &&
                isSpecialNode(parent, "start")
            ) {
                toDelete = parent
            }
            if (toDelete != null) {
                const range = rangeOfNode(toDelete)
                return textEditor.edit((editBuilder) => {
                    editBuilder.delete(range)
                })
            } else {
                return vscode.commands.executeCommand("deleteLeft")
            }
        }),
        clone: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const range = rangeOfNode(current)
            const position = new vscode.Position(current.endPosition.row, current.endPosition.column)
            const text = range.isSingleLine
                ? " " + current.text
                : "\n" + " ".repeat(current.startPosition.column) + current.text
            return textEditor.edit((editBuilder) => {
                editBuilder.insert(position, text)
            })
        }),
        slurp: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const forward = !isSelectionAtStartOfNode(textEditor, current)
            let next = forward ? current.nextNamedSibling : current.previousNamedSibling
            // Skip comments
            while (next && next.type === "comment") {
                next = forward ? next.nextNamedSibling : next.previousNamedSibling
            }
            if (next) {
                const cur = rangeOfNode(current)
                if (cur.end.character === 0) {
                    throw new Error("Illegal state")
                }
                const nex = rangeOfNode(next)
                const del = forward ? new vscode.Range(cur.end, nex.end) : new vscode.Range(nex.start, cur.start)
                // TODO: ensure space between children
                const text = textEditor.document.getText(del)
                const target = forward
                    ? new vscode.Position(cur.end.line, cur.end.character - 1)
                    : new vscode.Position(cur.start.line, cur.start.character + 1)
                return textEditor
                    .edit((editBuilder) => {
                        editBuilder.delete(del)
                        editBuilder.insert(target, text)
                    })
                    .then(() => {
                        const pos = forward ? nex.end : nex.start
                        textEditor.revealRange(new vscode.Range(pos, pos))
                    })
            }
        }),
        barf: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const forward = !isSelectionAtStartOfNode(textEditor, current)
            const child = forward ? current.lastNamedChild : current.firstNamedChild
            if (child && current.type !== "ERROR") {
                const cur = rangeOfNode(current)
                const furtherChild = forward ? child.previousNamedSibling : child.nextNamedSibling
                let upto
                if (furtherChild) {
                    const fur = rangeOfNode(furtherChild)
                    upto = forward ? fur.end : fur.start
                } else {
                    const chi = rangeOfNode(child)
                    upto = forward ? chi.start : chi.end
                }
                const del = forward
                    ? new vscode.Range(upto, new vscode.Position(cur.end.line, cur.end.character - 1))
                    : new vscode.Range(new vscode.Position(cur.start.line, cur.start.character + 1), upto)
                const text = textEditor.document.getText(del)
                const target = forward ? cur.end : cur.start
                return textEditor
                    .edit((editBuilder) => {
                        editBuilder.delete(del)
                        editBuilder.replace(target, text)
                    })
                    .then(() => {
                        const pos = forward
                            ? new vscode.Position(del.start.line, del.start.character + 1)
                            : new vscode.Position(del.end.line, del.end.character - 1)
                        textEditor.selection = new vscode.Selection(pos, pos)
                        textEditor.revealRange(textEditor.selection)
                    })
            }
        }),
        brackets: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            state.currentKey = "["
            return vscode.commands.executeCommand("lissy.pair")
        }),
        raise: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const parent = current.parent
            if (parent) {
                const targetIndex = isSelectionAtStartOfNode(textEditor, current)
                    ? parent.startIndex
                    : parent.startIndex + (current.endIndex - current.startIndex)
                const range = rangeOfNode(parent)
                return textEditor
                    .edit((editBuilder) => {
                        editBuilder.replace(range, current.text)
                    })
                    .then(() => {
                        selectAndRevealOffset(textEditor, targetIndex)
                    })
            }
        }),
        "move-up": withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const next = current.previousNamedSibling
            if (next) {
                const targetIndex = isSelectionAtStartOfNode(textEditor, current)
                    ? next.startIndex
                    : next.startIndex + (current.endIndex - current.startIndex)
                return textEditor
                    .edit((editBuilder) => {
                        editBuilder.replace(rangeOfNode(current), next.text)
                        editBuilder.replace(rangeOfNode(next), current.text)
                    })
                    .then(() => {
                        selectAndRevealOffset(textEditor, targetIndex)
                    })
            }
        }),
        "move-down": withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const next = current.nextNamedSibling
            if (next) {
                const targetIndex = isSelectionAtStartOfNode(textEditor, current)
                    ? next.endIndex - (current.endIndex - current.startIndex)
                    : next.endIndex
                return textEditor
                    .edit((editBuilder) => {
                        editBuilder.replace(rangeOfNode(next), current.text)
                        editBuilder.replace(rangeOfNode(current), next.text)
                    })
                    .then(() => {
                        selectAndRevealOffset(textEditor, targetIndex)
                    })
            }
        }),
        space: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const pos = textEditor.selection.start
            if (textEditor.selection.isEmpty && isSpecialAfterPosition(textEditor, pos)) {
                return textEditor
                    .edit((editBuilder) => {
                        editBuilder.insert(pos, " ")
                    })
                    .then(() => {
                        textEditor.selection = new vscode.Selection(pos, pos)
                        textEditor.revealRange(textEditor.selection)
                    })
            } else {
                return vscode.commands.executeCommand("default:type", { text: " " })
            }
        }),
        pair: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const pairs: { [key: string]: string } = {
                "(": ")",
                "[": "]",
                "{": "}",
                '"': '"',
                ":": "",
            }
            const left = state.currentKey
            if (left === undefined) {
                throw Error("Illegal state")
            }
            const right = pairs[left]
            if (right === undefined) {
                throw Error("Unmatched: " + left)
            }
            const current = findCurrentSexp(state, textEditor)
            if (textEditor.selection.isEmpty) {
                const ins = textEditor.selection.start
                const [text, offset] = !current.parent
                    ? [left + right, 1]
                    : positionEqualsPoint(ins, current.startPosition)
                    ? [left + right + " ", 1]
                    : positionEqualsPoint(ins, current.endPosition) &&
                      current.type !== "comment" &&
                      !current.text.endsWith("#")
                    ? [" " + left + right, 2]
                    : [left + right, 1]
                return textEditor
                    .edit((edit) => {
                        edit.insert(ins, text)
                    })
                    .then(() => {
                        const pos = new vscode.Position(ins.line, ins.character + offset)
                        textEditor.selection = new vscode.Selection(pos, pos)
                    })
            } else {
                const before = textEditor.selection.start
                const after = textEditor.selection.end
                return textEditor
                    .edit((edit) => {
                        edit.insert(before, left)
                        edit.insert(after, right)
                    })
                    .then(() => {
                        textEditor.selection = new vscode.Selection(
                            new vscode.Position(before.line, before.character + 1),
                            new vscode.Position(after.line, after.character + 1)
                        )
                    })
            }
        }),
        colon: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            if (!textEditor.selection.isEmpty) {
                state.currentKey = ":"
                return vscode.commands.executeCommand("lissy.pair")
            }
            const current = findCurrentSexp(state, textEditor)
            const ins = textEditor.selection.start
            const text = positionEqualsPoint(ins, current.startPosition) && isSpecialNode(current, "start") ? ": " : ":"
            return textEditor
                .edit((edit) => {
                    edit.insert(ins, text)
                })
                .then(() => {
                    const pos = new vscode.Position(ins.line, ins.character + 1)
                    textEditor.selection = new vscode.Selection(pos, pos)
                })
        }),
        comment: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const range = rangeOfNode(current)
            textEditor.selection = new vscode.Selection(range.start, range.end)
            return vscode.commands.executeCommand("editor.action.addCommentLine")
        }),
        select: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            const range = rangeOfNode(current)
            textEditor.selection = new vscode.Selection(range.start, range.end)
        }),
        "line-break": withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const position = textEditor.document.offsetAt(textEditor.selection.start)
            const current = findCurrentSexp(state, textEditor)
            // The cursor might be outside of the current (e.g. infront of opening parens),
            // thus, the "containing" sexp is either the current or its parent.
            const cont =
                positionEqualsPoint(textEditor.selection.start, current.startPosition) ||
                positionEqualsPoint(textEditor.selection.end, current.endPosition) ||
                current.type === "comment"
                    ? current.parent
                    : current
            const indent =
                !cont || !cont.parent || cont.type === "string"
                    ? 0
                    : cont.text.charAt(0) === "[" || cont.text.charAt(0) === "{"
                    ? cont.startPosition.column + 1
                    : cont.text.charAt(0) === "("
                    ? cont.startPosition.column + listIndentation(state, cont, position)
                    : cont.startPosition.column
            textEditor.edit((edit) => {
                edit.replace(textEditor.selection, "\n" + " ".repeat(indent))
            })
        }),
        indent: withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexpAfter(state, textEditor)
            const indents = indentsOfNode(state, current)
            textEditor.edit((edit) => {
                let i = 0
                for (const indent of indents) {
                    i++
                    const row = current.startPosition.row + i
                    const line = textEditor.document.lineAt(row)
                    const currentIndent = line.firstNonWhitespaceCharacterIndex
                    if (indent !== currentIndent) {
                        const range = new vscode.Range(
                            new vscode.Position(row, 0),
                            new vscode.Position(row, currentIndent)
                        )
                        edit.replace(range, " ".repeat(indent))
                    }
                }
            })
        }),

        // --- Extra -----------------------------------------------------------

        "print-syntax-tree": withTextEditor((textEditor: vscode.TextEditor, ...args: unknown[]) => {
            const current = findCurrentSexp(state, textEditor)
            console.log("\n[Lissy]", current.toString()) // TODO: show differently; inline?
        }),
    }
}
