/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import * as vscode from "vscode"
import { State } from "./state"

// Default: Lispy keymap
const lispyDefaultGlobals: { [key: string]: string } = {
    "]": "lissy.forward",
    "[": "lissy.backward",
    ")": "::right-nostring",
    "}": "lissy.brackets",
    "(": "lissy.pair",
    "{": "lissy.pair",
    '"': "lissy.pair",
    ":": "lissy.colon",
    "\n": "lissy.line-break",
    // DEL does not insert anything, so it has to be bound as a keyboard shortcut
    // DEL: 'lissy.delete-backward'
}

// Only active, when point is special, e.g. before or after parens
const lispyDefaultSpecials: { [key: string]: string } = {
    // Movement
    l: "lissy.right", // forward up
    h: "lissy.left", // backward up
    j: "lissy.down", // forward
    k: "lissy.up", // backward
    d: "lissy.different",
    f: "lissy.flow",
    b: "lissy.back",
    // Modification
    c: "lissy.clone",
    ">": "lissy.slurp",
    "<": "lissy.barf",
    r: "lissy.raise",
    w: "lissy.move-up",
    s: "lissy.move-down",
    " ": "lissy.space",
    ";": "lissy.comment",
    i: "lissy.indent",
    // Other
    u: "undo",
    // Extra (non-lispy commands)
    p: "lissy.print-syntax-tree",
    ".": "lissy.select", // select the current expression
}

function getKeymap(defaults: { [key: string]: string }, configSection: string): Map<string, string> {
    const map = new Map()

    function setEntries(obj: object) {
        Object.entries(obj).forEach(([key, value]) => {
            if (value != null) {
                map.set(key, value)
            } else {
                map.delete(key)
            }
        })
    }

    const config = vscode.workspace.getConfiguration("lissy")

    // TODO: different base keymaps?
    if (config.get("base-keymap") === "lispy") {
        setEntries(defaults)
    }

    const configured = config.get(configSection, {})
    setEntries(configured)

    return map
}

export function getGlobalKeymap(): Map<string, string> {
    return getKeymap(lispyDefaultGlobals, "global-keys")
}

export function getSpecialKeymap(): Map<string, string> {
    return getKeymap(lispyDefaultSpecials, "special-keys")
}
